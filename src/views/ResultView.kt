package com.simfootball.isfl.views

import kotlinx.html.*

fun HTML.resultView(output: String) {
    head {
        title { +"Update Auditor" }
    }
    body {
        pre { +output }
    }
}
