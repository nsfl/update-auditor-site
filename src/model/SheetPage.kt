package model

class SheetPage(
    val playerName: String,
    val team: Team,
    val tpe: Int,
    val position: String,
    val strength: Int,
    val agility: Int,
    val arm: Int,
    val intelligence: Int,
    val throwingAccuracy: Int,
    val tackling: Int,
    val speed: Int,
    val hands: Int,
    val passBlocking: Int,
    val runBlocking: Int,
    val endurance: Int,
    val kickPower: Int,
    val kickAccuracy: Int,
    val competitiveness: Int,
    val archetype: String
)