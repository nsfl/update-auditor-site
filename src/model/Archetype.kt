package model;

fun getArchetypeFromSheet(archetype: String) = ArchetypeEnum.values().find { it.archetypeSheet == archetype}

enum class ArchetypeEnum(val archetypeTracker: String, val archetypeSheet: String) {
    RB_FULLBACK("Fullback","FB-Fullback"),
    RB_POWER_BACK("Power Back","RB-Power Back"),
    RB_RECEIVING_BACK("Receiving Back","RB-Receiving Back"),
    RB_SPEED_BACK("Speed Back","RB-Speed Back"),

    TE_BLOCKING_TE("Blocking TE","TE-Blocking TE"),
    TE_POSSESSION_TE("Possession TE","TE-Possession TE"),
    TE_VERTICAL_THREAT("Vertical Threat","TE-Vertical Threat"),

    QB_GUNSLINGER("Gunslinger","QB-Gunslinger QB"),
    QB_POCKET_PASSER("Pocket Passer","QB-Pocket Passer"),
    QB_FIELD_GENERAL("Field General","QB-Field General"),
    QB_SCRAMBLER("Scrambler","QB-Scrambler"),
    QB_MOBILE("Mobile","QB-Mobile"),

    OL_MAULER("Mauler","OL-Mauler"),
    OL_BALANCED_LINEMAN("Balanced Lineman","OL-Balanced Lineman"),
    OL_ATHLETIC_LINEMAN("Athletic Lineman","OL-Athletic Lineman"),

    WR_SLOT_RECEIVER("Slot Receiver","WR-Slot Receiver"),
    WR_POSSESSION_RECEIVER("Possession Receiver","WR-Possession Receiver"),
    WR_SPEED_RECEIVER("Speed Receiver","WR-Speed Receiver"),

    DT_INTERIOR_RUSHER("Interior Rusher","DT-Interior Rusher"),
    DT_ALL_PURPOSE_DT("All-Purpose DT","DT-All-Purpose DT"),
    DT_NOSE_TACKLE("Nose Tackle","DT-Nose Tackle"),

    DE_SPEED_RUSHER("Speed Rusher","DE-Speed Rusher"),
    DE_POWER_RUSHER("Power Rusher","DE-Power Rusher"),
    DE_RUN_STUFFER("Run Stuffer","DE-Run Stuffer"),

    LB_COVERAGE_LB("Coverage LB","LB-Coverage LB"),
    LB_VERSATILE_LB("Versatile LB","LB-Versatile LB"),
    LB_PASS_RUSHER("Pass Rusher","LB-Pass Rusher"),

    CB_SLOT_CORNER("Slot Corner","CB-Slot Corner"),
    CB_PHYSICAL_CORNER("Physical Corner","CB-Physical Corner"),
    CB_COVER_CORNER("Cover Corner","CB-Cover Corner"),

    S_ENFORCER("Enforcer","S-Enforcer"),
    S_BALL_HAWK("Ball Hawk","S-Ball Hawk"),
    S_CENTER_FIELDER("Center Fielder","S-Center Fielder"),

    KP_POWER("Power","K-Power"),
    KP_ACCURATE("Accurate","K-Accurate")
}
