package model

class Team(val name: String, val forumId: String, val simId: String)

fun getNSFLTeamList() = NSFLTeam.values().map { Team(it.name, it.forumId, it.simId) }

fun getDSFLTeamList() = DSFLTeam.values().map { Team(it.name, it.forumId, it.simId) }

fun getTeamById(id: String): String{
    var team: String
    team = NSFLTeam.values().find { it.forumId == id}?.name ?: ""

    if (team.isEmpty()) {
        team = DSFLTeam.values().find { it.forumId == id}?.name ?: "TEAM NOT FOUND"
    }

    return team
}

enum class NSFLTeam(val forumId: String, val simId: String) {
    BALTIMORE_HAWKS("82", "BAL"),
    CHICAGO_BUTCHERS("160", "CHI"),
    BERLIN_FIRE_SALAMANDERS("368","BER"),
    COLORADO_YETI("66", "COL"),
    PHILADELPHIA_LIBERTY("144", "PHI"),
    YELLOWKNIFE_WRAITHS("68", "YKW"),
    ARIZONA_OUTLAWS("85", "ARI"),
    AUSTIN_COPPERHEADS("229", "AUS"),
    NEW_ORLEANS_SECOND_LINE("147", "NOLA"),
    NEW_YORK_SILVERBACKS("371","NYS"),
    ORANGE_COUNTY_OTTERS("64", "OCO"),
    SAN_JOSE_SABERCATS("62", "SJS"),
    SARASOTA_SAILFISH("306", "SAR"),
    HONOLULU_HAHALUA("308", "HON")
}

enum class DSFLTeam(val forumId: String, val simId: String) {
    BONDI_BEACH_BUCCANEERS("195", "BBB"),
    KANSAS_CITY_COYOTES("193", "KCC"),
    PORTLAND_PYTHONS("199", "POR"),
    NORFOLK_SEAWOLVES("197", "NOR"),
    MINNESOTA_GREY_DUCKS("191", "MINN"),
    TIJUANA_LUCHADORES("189", "TIJ"),
    DALLAS_BIRDDOGS("297", "DAL"),
    LONDON_ROYALS("234", "LON")
}
