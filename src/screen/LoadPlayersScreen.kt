package screen

import model.*
import javax.swing.JPopupMenu

object LoadPlayersScreen {

    fun process(csv: String, auditingDSFL: Boolean): String {

        val teamList = if (auditingDSFL) getDSFLTeamList() else getNSFLTeamList()

        val specialPlayerNames = arrayListOf(
            Pair("Troy Humuhumunukunukuapua'a", "Troy Humuhumunukunukuāpuaʻa"),
            Pair("Marcella Toriki", "Marcella Tōriki"),
            Pair("Bjorn Ironside", "Bjørn Ironside"),
            Pair("Ke'oke'o Kane-Maika'i", "Keʻokeʻo Kāne-Maikaʻi"),
            Pair("Bane Ka'ana'ana", "Bane Kaʻanāʻanā"),
            Pair("Momona Keiki-Kane", "Momona Keiki-Kāne"),
            Pair("Mai Fukushu", "Mai Fukushū"),
            Pair("Adelie de Pengu", "Adélie de Pengu"),
            Pair("Three Foot Jeffrey","3' Jeffrey"),
            Pair("Sirdsvaldis Miglaskems","Sirdsvaldis Miglasķēms")
        )

        val sheetPageList = csv
            .split("\n").let { it.subList(1, it.size) }
            .map { player ->

                val attributes = player.split(",")

                var playerName = attributes[0] + " " + attributes[1].replace(" (R)", "").replace(" (C)","")
                specialPlayerNames.firstOrNull { it.first == playerName }?.let {
                    playerName = it.second
                }

                SheetPage(
                    playerName = playerName,
                    team = teamList.firstOrNull { it.simId == attributes[6] } ?: Team("", "", ""),
                    tpe = attributes[7].toIntOrNull() ?: 0,
                    position = attributes[8],
                    strength = attributes[10].toIntOrNull() ?: 0,
                    agility = attributes[11].toIntOrNull() ?: 0,
                    arm = attributes[12].toIntOrNull() ?: 0,
                    intelligence = attributes[13].toIntOrNull() ?: 0,
                    throwingAccuracy = attributes[14].toIntOrNull() ?: 0,
                    tackling = attributes[15].toIntOrNull() ?: 0,
                    speed = attributes[16].toIntOrNull() ?: 0,
                    hands = attributes[17].toIntOrNull() ?: 0,
                    passBlocking = attributes[18].toIntOrNull() ?: 0,
                    runBlocking = attributes[19].toIntOrNull() ?: 0,
                    endurance = attributes[20].toIntOrNull() ?: 0,
                    kickPower = attributes[21].toIntOrNull() ?: 0,
                    kickAccuracy = attributes[22].toIntOrNull() ?: 0,
                    competitiveness = attributes[23].toIntOrNull()?: 0,
                    archetype = attributes[9]
                )
            }

        return AuditUpdatesScreen.start(sheetPageList, teamList)
    }
}
