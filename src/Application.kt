package com.simfootball.isfl

import com.simfootball.isfl.views.inputView
import com.simfootball.isfl.views.resultView
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.html.respondHtml
import io.ktor.http.content.resources
import io.ktor.http.content.static
import io.ktor.request.receiveParameters
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import screen.LoadPlayersScreen

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    routing {
        get("/") { call.respondHtml { inputView() } }

        post("/") {
            val params = call.receiveParameters()
            val csv = params["csv"]!!
            val dsfl = params["league"]!! == "DSFL"
            call.respondHtml { resultView(LoadPlayersScreen.process(csv, dsfl)) }
        }

        // Static feature. Try to access `/static/ktor_logo.svg`
        static("/static") {
            resources("static")
        }
    }
}

