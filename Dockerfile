FROM openjdk:14-buster AS builder

RUN mkdir /workspace
WORKDIR /workspace

COPY ./ /workspace
RUN chmod +x ./gradlew
RUN ./gradlew clean shadowJar --no-daemon

######################################

FROM openjdk:14-buster

RUN mkdir /app

COPY --from=builder /workspace/build/libs/update-auditor-ktor-0.0.1-all.jar /app/update-auditor-ktor.jar
WORKDIR /app

CMD ["java", "-server", "-jar", "update-auditor-ktor.jar"]
